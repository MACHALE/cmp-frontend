import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder,Validators } from '@angular/forms';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 loginForm:FormGroup;
  username: string;
  password: string;
  returnUrl:string;


  constructor(private formBuilder:FormBuilder,private router:Router,private route:ActivatedRoute) {} 

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';
  }
  
  get form() {
    return this.loginForm.controls;
  }
  
  login() { 
    if(this.form.username.value == 'admin' && this.form.password.value=="admin"){
    console.log("hello"+this.username);
    this.router.navigate([this.returnUrl]); 
    }    
}
signup() { 
  this.router.navigateByUrl('/signup'); 
}
forgotpass(){
  this.router.navigateByUrl('/forgotpass');
}

}
