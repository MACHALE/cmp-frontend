import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http:HttpClient) { }
  login(username: string, password: string) {

    return this.http.get(`/cmp` + username + '&password=' + password)
      .pipe(
        map(user => {
          if (user) {
            console.log(user);
            localStorage.setItem('User', JSON.stringify(user));
          }

          return user;
        })
      );
  }
}
