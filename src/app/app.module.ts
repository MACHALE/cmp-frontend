import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { NavComponent } from './nav/nav.component';
import {FormsModule, ReactiveFormsModule } from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {  MaterialModule } from './sharedModules/material.module';
import { SidenavComponent } from './sidenav/sidenav.component';
import { AccountFormComponent } from './account-form/account-form.component';
import { SignupComponent } from './signup/signup.component';
import { AccountRecoveryComponent } from './account-recovery/account-recovery.component';
import { ChangePasswordComponent } from './change-password/change-password.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    NavComponent,
    SidenavComponent,
    AccountFormComponent,
    SignupComponent,
    AccountRecoveryComponent,
    ChangePasswordComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [AccountFormComponent]
})
export class AppModule { }
