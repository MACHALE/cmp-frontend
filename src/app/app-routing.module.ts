import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { NavComponent } from './nav/nav.component';
import { AuthGuard } from './auth.guard';
import { SignupComponent } from './signup/signup.component';
import { AccountRecoveryComponent } from './account-recovery/account-recovery.component';
import { ChangePasswordComponent } from './change-password/change-password.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'signup',component: SignupComponent},
  //{path:'home',canActivate: [AuthGuard],component:HomeComponent}
  {path:'home',component:HomeComponent},
  {path:'forgotpass',component:AccountRecoveryComponent},
  {path:'profile',component:ChangePasswordComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
