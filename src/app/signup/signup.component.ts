import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
signUpForm:FormGroup;
firstname:string;
lastname:string;
email:string;
password:string;
cnfpassword:string;
returnUrl:string;
  constructor(private formBuilder:FormBuilder,private router:Router,private route:ActivatedRoute) { }

  ngOnInit() {
    this.signUpForm = this.formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['',Validators.required],
      email: ['',Validators.required],
      password: ['',Validators.required],
      cnfpassword: ['', Validators.required]
    });
  }
  get form() {
    return this.signUpForm.controls;
  }
  signup() { 
    console.log("hello"+this.firstname);  
}

}
