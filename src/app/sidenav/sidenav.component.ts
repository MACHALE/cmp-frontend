import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AccountFormComponent } from '../account-form/account-form.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {
  public show:boolean = false; 
  constructor(public dialog:MatDialog,private router:Router) { }

  ngOnInit() {
  }
  accountForm(): void {
    const dialogRef = this.dialog.open(AccountFormComponent, {
      panelClass: 'custom-dialog-container'
      
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
    });
  }
  toggle() {
    this.show = !this.show;
  }
  logout() {
    localStorage.removeItem('User');
    this.router.navigate(['/']);
  }
  changepass() {
    
    this.router.navigateByUrl('/profile');
  }

}
