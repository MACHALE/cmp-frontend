import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-account-recovery',
  templateUrl: './account-recovery.component.html',
  styleUrls: ['./account-recovery.component.css']
})
export class AccountRecoveryComponent implements OnInit {
recoveryForm:FormGroup;
username:string;
  constructor(private frombuilder:FormBuilder) { }

  ngOnInit() {
    this.recoveryForm=this.frombuilder.group({
      username:['',Validators.required]
    })
  }

}
