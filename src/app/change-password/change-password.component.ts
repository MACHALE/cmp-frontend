import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
changePassForm:FormGroup;
currentpass:string;
newpass:string;
cnfpass:string;
  constructor(private formBuilder:FormBuilder) { }

  ngOnInit() {
    this.changePassForm=this.formBuilder.group({
      currentpass:['',Validators.required],
      newpass:['',Validators.required],
      cnfpass:['',Validators.required]
    })
  }
  submit(){
    
  }

}
